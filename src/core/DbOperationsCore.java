package core;

public interface DbOperationsCore {
    void readSHNGuests();
    void readSTCGuests();
    
    // not implemented as not required by question
	/*
	 * void insertGuests(Guest g); 
	 * void updateGuests(int id, String original, String updated); 
	 * void deleteGuests(int id); 
	 */
}
