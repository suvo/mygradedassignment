package core;

public interface PopulateTableCore {
    void read();
    void insert();
    void update();
    void delete();
}
